<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Thông tin về Tor">

<!ENTITY aboutTor.viewChangelog.label "Xem nhật kí thay dổi">

<!ENTITY aboutTor.ready.label "Truy cập Internet. Một cách riêng tư.">
<!ENTITY aboutTor.ready2.label "Bạn đã sẵn sàng cho trải nghiệm duyệt web riêng tư nhất trên thế giới.">
<!ENTITY aboutTor.failure.label "Có Lỗi Xảy Ra!">
<!ENTITY aboutTor.failure2.label "Tor không hoạt động trên trình duyệt này.">

<!ENTITY aboutTor.search.label "Tìm kiếm với DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Những câu hỏi?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Xem qua Hướng dẫn sử dụng trình duyệt Tor »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Hướng dẫn sử dụng trình duyệt Tor">

<!ENTITY aboutTor.tor_mission.label "The Tor Project là một tổ chức phi lợi nhuận 501(c)(3) của Hoa Kỳ nhằm thúc đẩy quyền con người và tự do bằng cách tạo và triển khai các công nghệ đảm bảo quyền riêng tư và ẩn danh miễn phí và dựa trên mã nguồn mở, hỗ trợ tính sẵn có và sử dụng không hạn chế của chúng, và giúp phổ biến hơn nữa sự hiểu biết về tính khoa học của những công nghệ này đối với cộng đồng.">
<!ENTITY aboutTor.getInvolved.label "Tham gia »">

<!ENTITY aboutTor.newsletter.tagline "Nhận thông tin mới nhất từ Tor được gửi tới hộp thư của bạn.">
<!ENTITY aboutTor.newsletter.link_text "Đăng kí nhận tin tức từ Tor.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor miễn phí là nhờ sự ủng hộ của những người như bạn.">
<!ENTITY aboutTor.donationBanner.buttonA "Đóng góp Ngay bây giờ">

<!ENTITY aboutTor.alpha.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.alpha.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.alpha.bannerLink "Report a bug on the Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.nightly.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "Report a bug on the Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "Donate Ngay bây giờ">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Sự đóng góp của bạn sẽ được kết hợp với Friends of Tor.">

<!ENTITY torsettings.dialog.title "Tor tinklo nustatymai">
<!ENTITY torsettings.wizard.title.default "Prisijungti prie Tor">
<!ENTITY torsettings.wizard.title.configure "Tor tinklo nustatymai">
<!ENTITY torsettings.wizard.title.connecting "Užmezgiamas ryšys">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tor Naršyklės kalba">
<!ENTITY torlauncher.localePicker.prompt "Pasirinkite kalbą.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Norėdami prisijungti prie Tor, spustelėkite &quot;Prisijungti&quot;.">
<!ENTITY torSettings.configurePrompt "Spustelėkite &quot;Konfigūruoti&quot;, norėdami reguliuoti tinklo nustatymus, jeigu esate šalyje, kuri cenzūruoja Tor (tokioje kaip Egiptas, Kinija, Turkija) arba jeigu jungiatės iš privačiojo tinklo, kuris reikalauja įgaliotojo serverio.">
<!ENTITY torSettings.configure "Konfigūruoti">
<!ENTITY torSettings.connect "Prisijungti">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Laukiama, kol bus paleistas Tor…">
<!ENTITY torsettings.restartTor "Paleisti Tor iš naujo">
<!ENTITY torsettings.reconfigTor "Konfigūruoti iš naujo">

<!ENTITY torsettings.discardSettings.prompt "Jūs esate sukofigūravę Tor tinklų tiltus arba esate įrašę vietinio įgaliotojo serverio nustatymus.&#160; Tam, kad galėtumėte tiesiogiai prisijungti prie Tor tinklo, šie nustatymai privalo būti pašalinti.">
<!ENTITY torsettings.discardSettings.proceed "Šalinti nustatymus ir prisijungti">

<!ENTITY torsettings.optional "Nebūtina">

<!ENTITY torsettings.useProxy.checkbox "Prisijungimui prie interneto aš naudoju įgaliotąjį serverį">
<!ENTITY torsettings.useProxy.type "Įgaliotojo serverio tipas">
<!ENTITY torsettings.useProxy.type.placeholder "pasirinkite įgaliotojo serverio tipą">
<!ENTITY torsettings.useProxy.address "Adresas">
<!ENTITY torsettings.useProxy.address.placeholder "IP adresas arba serverio vardas">
<!ENTITY torsettings.useProxy.port "Prievadas">
<!ENTITY torsettings.useProxy.username "Naudotojo vardas">
<!ENTITY torsettings.useProxy.password "Slaptažodis">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Šis kompiuteris jungiasi per užkardą, kuri leidžia jungtis tik prie tam tikrų prievadų">
<!ENTITY torsettings.firewall.allowedPorts "Leidžiami prievadai">
<!ENTITY torsettings.useBridges.checkbox "Mano šalyje Tor yra cenzūruojamas">
<!ENTITY torsettings.useBridges.default "Select a Built-In Bridge">
<!ENTITY torsettings.useBridges.default.placeholder "pasirinkite tinklų tiltą">
<!ENTITY torsettings.useBridges.bridgeDB "Prašyti tinklų tilto iš torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Įveskite ženklus iš paveikslėlio">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Gaukite naują iššūkį">
<!ENTITY torsettings.useBridges.captchaSubmit "Pateikti">
<!ENTITY torsettings.useBridges.custom "Pateikti man žinomą tinklų tiltą">
<!ENTITY torsettings.useBridges.label "Įveskite tinklų tilto informaciją iš patikimo šaltinio.">
<!ENTITY torsettings.useBridges.placeholder "tipas adresas:prievadas (kiekvienoje eilutėje po vieną)">

<!ENTITY torsettings.copyLog "Kopijuoti Tor žurnalą į iškarpinę">

<!ENTITY torsettings.proxyHelpTitle "Įgaliotojo serverio žinynas">
<!ENTITY torsettings.proxyHelp1 "Vietinis įgaliotasis serveris gali būti reikalingas, jungiantis per kompanijos, mokyklos ar universiteto tinklą.&#160;Jeigu nesate tikri ar įgaliotasis serveris yra reikalingas, žiūrėkite interneto nustatymus kitoje naršyklėje arba patikrinkite savo sistemos tinklo nustatymus.">

<!ENTITY torsettings.bridgeHelpTitle "Tiltų tinklo retransliavimo žinynas">
<!ENTITY torsettings.bridgeHelp1 "Tinklų tiltai yra neregistruoti retransliavimai, kurie apsunkina ryšių prie Tor tinklo blokavimą.&#160; Kiekvienas tinklų tilto tipas naudoja kitokį būdą išvengti cenzūros.&#160; Obfs tinklų tiltai paverčia jūsų srautą panašų į atsitiktinį triukšmą, o meek tinklų tiltai paverčia jūsų srautą, kad jis atrodytų taip, lyg jungtumėtės prie tos paslaugos, o ne prie Tor.">
<!ENTITY torsettings.bridgeHelp2 "Dėl to, kaip tam tikros šalys bando blokuoti Tor, tam tikri tinklų tiltai vienose šalyse veikia, o kitose neveikia.&#160; Jeigu nesate tikri, kurie tinklų tiltai jūsų šalyje veikia, apsilankykite torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Palaukite, kol mes užmegsime ryšį su Tor tinklu.&#160; Tai gali užtrukti kelias minutes.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Ryšys">
<!ENTITY torPreferences.torSettings "Tor nustatymai">
<!ENTITY torPreferences.torSettingsDescription "Tor Naršyklė nukreipia jūsų srautą per Tor tinklą, veikiantį po visą pasaulį išsidėsčiusių tūkstančių savanorių dėka." >
<!ENTITY torPreferences.learnMore "Sužinoti daugiau">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internetas:">
<!ENTITY torPreferences.statusInternetTest "Test">
<!ENTITY torPreferences.statusInternetOnline "Pasiekiama(-s)">
<!ENTITY torPreferences.statusInternetOffline "Nepasiekiama">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "Prisijungta">
<!ENTITY torPreferences.statusTorNotConnected "Neprisijungta">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "Sužinoti daugiau">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Greita pradžia">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Visuomet jungtis automatiškai">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Tinklų tiltai">
<!ENTITY torPreferences.bridgesDescription "Tinklo tiltai padeda jums pasiekti Tor tinklą, vietose kur Tor užblokuotas. Priklausomai nuo to kur esate, vienas tiltas gali veikti geriau nei kitas.">
<!ENTITY torPreferences.bridgeLocation "Jūsų vieta">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatiškai">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can save one or more bridges, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Šalinti">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "Nukopijuota!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "Užklausti tinklų tiltą…">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Išplėstiniai">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Nustatymai…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "Rodyti žurnalus…">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "Šis veiksmas negalės būti atšauktas.">
<!ENTITY torPreferences.cancel "Atsisakyti">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Užklausti tinklų tiltą…">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Susisiekiama su BridgeDB. Palaukite.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Norėdami užklausti tinklų tiltą, išspręskite saugos kodą.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Sprendimas neteisingas. Bandykite dar kartą.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Įveskite tinklų tilto informaciją iš patikimo šaltinio">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor žurnalai">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Neprisijungta">
<!ENTITY torConnect.connectingConcise "Jungiamasi…">
<!ENTITY torConnect.tryingAgain "Bandoma dar kartą…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.noInternetDescription "This could be due to a connection issue rather than Tor being blocked. Check your Internet connection, proxy and firewall settings before trying again.">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Bandoma dar vieną kartą…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.finalError "Tor Browser still cannot connect">
<!ENTITY torConnect.finalErrorDescription "Despite its best efforts, connection assist was not able to connect to Tor. Try troubleshooting your connection and adding a bridge manually instead.">
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automatiškai">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Configure Connection…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "Bandyti dar kartą">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor Naršyklei nepavyko užmegzti ryšio su Tor tinklu">
<!ENTITY torConnect.yourLocation "Jūsų vieta">
<!ENTITY torConnect.tryBridge "Try a Bridge">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Nepavyko nustatyti naudotojo šalies">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">

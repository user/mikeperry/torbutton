<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Acerca de Tor">

<!ENTITY aboutTor.viewChangelog.label "Ver registro de modificaciones.">

<!ENTITY aboutTor.ready.label "Explora. En privado.">
<!ENTITY aboutTor.ready2.label "Ahora estás listo/a para experimentar la navegación más privada del mundo.">
<!ENTITY aboutTor.failure.label "¡Algo fue mal!">
<!ENTITY aboutTor.failure2.label "Tor no está funcionando en este navegador.">

<!ENTITY aboutTor.search.label "Buscar con DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "¿Preguntas?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Comprobar nuestro Manual del Tor Browser">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Manual del Tor Browser">

<!ENTITY aboutTor.tor_mission.label "El proyecto Tor es una organización sin fines de lucro definida legalmente en Estados Unidos como 501(c)(3), avanzando libertades y derechos humanos mediante la creación y despliegue de tecnologías de anonimato y privacidad sin costo y de fuente abierta, apoyando su disponibilidad y uso sin restricciones y ampliando su entendimiento científico y popular.">
<!ENTITY aboutTor.getInvolved.label "Involúcrate »">

<!ENTITY aboutTor.newsletter.tagline "Recibe las últimas noticias de Tor directamente en tu bandeja de entrada.">
<!ENTITY aboutTor.newsletter.link_text "Inscríbete en Tor News.">
<!ENTITY aboutTor.donationBanner.freeToUse "Se puede usar Tor libremente por las donaciones de personas como tu.">
<!ENTITY aboutTor.donationBanner.buttonA "Dona ahora.">

<!ENTITY aboutTor.alpha.ready.label "Prueba. Minuciosamente.">
<!ENTITY aboutTor.alpha.ready2.label "Estás listo para probar la experiencia de navegación más privada del mundo.">
<!ENTITY aboutTor.alpha.bannerDescription "El Navegador Tor Alpha es una versión inestable del Navegador Tor que puedes usar para obtener una visión previa de las nuevas funciones, probar su rendimiento y proporcionar comentarios antes del lanzamiento.">
<!ENTITY aboutTor.alpha.bannerLink "Informar sobre un error en el Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "Prueba. Minuciosamente.">
<!ENTITY aboutTor.nightly.ready2.label "Estás listo para probar la experiencia de navegación más privada del mundo.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly es una versión inestable de Tor Browser que puedes usar para obtener una vista previa de nuevas funciones, probar su rendimiento y proporcionar comentarios antes del lanzamiento.">
<!ENTITY aboutTor.nightly.bannerLink "Informar sobre un error en el Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "ANIMADO POR LA PRIVACIDAD:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTENCIA">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CAMBIO">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "LIBERTAD">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "DONA AHORA">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Tu donación será igualada por Amigos de Tor hasta $100.000.">
